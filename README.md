# Bill calculator

## Installation

```
$ yarn install
```

## Start program
```
$ yarn run start
```

## Run testing script
```
$ yarn run test
```
