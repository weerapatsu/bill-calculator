import React, { Component } from 'react';
import Form from './components/Form';
import './styles/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <Form />
          </div>
        </div>{/* /.container */}
      </div>
    );
  }
}

export default App;
