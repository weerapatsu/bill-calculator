import React from 'react';
import { calculateDiscountPrice, getPercentDiscount } from '../../components/Calculation';

it('calculate discount price', () => {
  expect(calculateDiscountPrice(500, 50)).toEqual(250);
  expect(calculateDiscountPrice(1000, 10)).toEqual(100);
});

it('get percent discount', () => {
  expect(getPercentDiscount(500, 'LUCKY ONE', 2)).toEqual(15);
  expect(getPercentDiscount(6200, '', 7)).toEqual(25);
  expect(getPercentDiscount(4000, '4PAY3', 4)).toEqual(25);
});
