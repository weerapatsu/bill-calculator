import React from 'react';
import {shallow} from 'enzyme';
import Form from '../../components/Form';

describe('<Form />', () => {
  it('submit button should shown on the page', () => {
    const wrapper = shallow(<Form />);
    expect(wrapper.find('.btn-success')).toHaveLength(1);
  });
});
