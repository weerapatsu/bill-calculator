import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatNumber } from '../lib';

class Bill extends Component {
  render() {
    return (
      <div>
        <div className="form-group">
          <label>Discount</label>&nbsp;
          <span className="bg-warning">{this.props.percentDiscount}</span> %
        </div>

        <div className="form-group">
          <label>Total Bill Amount</label>
          <div className="alert alert-info">{formatNumber(this.props.bill)}</div>
        </div>
      </div>
    );
  }
}

Bill.propTypes = {
  percentDiscount: PropTypes.number.isRequired,
  bill: PropTypes.number.isRequired,
};

export default Bill;
