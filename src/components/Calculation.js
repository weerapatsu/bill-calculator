/**
 *
 * @param billBeforeDiscount
 * @param percentDiscount
 * @returns {number}
 */
export function calculateDiscountPrice(billBeforeDiscount, percentDiscount) {
  return (billBeforeDiscount * (percentDiscount / 100));
}

/**
 *
 * @param billBeforeDiscount
 * @param promotionCode
 * @param customerNumber
 * @returns {number}
 */
export function getPercentDiscount(billBeforeDiscount, promotionCode, customerNumber) {
  let percentDiscount = 0;

  if (billBeforeDiscount > 1000 || promotionCode === 'LUCKY ONE') {
    percentDiscount = 15;
  }

  if (customerNumber === 2 && promotionCode === 'LUCKY TWO') {
    percentDiscount = 20;
  }

  if (customerNumber === 4 && promotionCode === '4PAY3') {
    percentDiscount = 25;
  }

  if (billBeforeDiscount > 6000) {
    percentDiscount = 25;
  }

  return percentDiscount;
}

