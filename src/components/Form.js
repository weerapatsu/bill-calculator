import React, { Component } from 'react';
import BillResult from './BillResult';
import { formatNumber } from '../lib';
import { calculateDiscountPrice, getPercentDiscount } from './Calculation';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pricePerPerson: 549,
      customerNumber: 0,
      promotionCode: '',
      billBeforeDiscount: 0,
      percentDiscount: 0,
      bill: 0,
      isCalculated: false,
    };

    this.customerNumberList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.promotionCodeList = ['LUCKY ONE', '4PAY3', 'LUCKY TWO'];
    this.customerOnchange = this.customerOnchange.bind(this);
    this.promotionCodeChange = this.promotionCodeChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  promotionCodeChange(e) {
    const promotionCode = e.target.value;
    const isCalculated = false;
    this.setState({ promotionCode, isCalculated });
  }

  customerOnchange(e) {
    const customerNumber = Number(e.target.value);
    const billBeforeDiscount = customerNumber * this.state.pricePerPerson;
    const isCalculated = false;

    this.setState({ customerNumber, billBeforeDiscount, isCalculated });
  }

  submit() {
    const percentDiscount = getPercentDiscount(
      this.state.billBeforeDiscount,
      this.state.promotionCode,
      this.state.customerNumber,
    );

    const bill = this.state.billBeforeDiscount - calculateDiscountPrice(
      this.state.billBeforeDiscount,
      percentDiscount,
    );

    const isCalculated = true;

    this.setState({ bill, percentDiscount, isCalculated });
  }

  render() {
    let bill = null;
    if (this.state.isCalculated) {
      bill = <BillResult percentDiscount={this.state.percentDiscount} bill={this.state.bill} />;
    }

    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
              <h1 className="page-header">
                Bill Calculator
              </h1>
              <div className="form-group">
                <label htmlFor="customer-number">Number of customer</label>
                <select
                  id="customer-number"
                  className="form-control"
                  onChange={this.customerOnchange}
                  value={this.state.customerNumber}
                >
                  <option value="">Select</option>
                  {this.customerNumberList.map(number =>
                    <option value={number} key={number}>{number}</option>)}
                </select>
              </div>

              <div className="form-group">
                <label>Bill Amount Before Discount</label>
                <div>{formatNumber(this.state.billBeforeDiscount)}</div>
              </div>

              <div className="form-group">
                <label htmlFor="code">Promotion Code</label>
                <select
                  id="code"
                  className="form-control"
                  onChange={this.promotionCodeChange}
                  value={this.state.promotionCode}
                >
                  <option value="">Select</option>
                  {this.promotionCodeList.map(code =>
                    <option value={code} key={code}>{code}</option>)}
                </select>
              </div>

              <div className="form-group">
                <button
                  type="button"
                  className="btn btn-success"
                  onClick={this.submit}
                >
                  Calculate
                </button>
              </div>

              {bill}
            </div>{/* /.col-md-12 */}
          </div>
        </div>{/* /.container */}
      </div>
    );
  }
}

export default Form;
